package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AddTaskToDeviceDto;
import pl.sda.computerservice.model.dto.AdminModifyTaskDto;
import pl.sda.computerservice.model.dto.ModifyTaskDto;
import pl.sda.computerservice.repository.ClientRepository;
import pl.sda.computerservice.repository.DeviceRepository;
import pl.sda.computerservice.repository.TaskRespository;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional
public class TaskService {

    @Autowired
    private TaskRespository taskRespository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ClientRepository clientRepository;

    public Optional<Device> addTaskToDevice(AddTaskToDeviceDto dto) {
        return addTaskToDevice(dto.getDescription(), dto.getDeviceId());
    }

    private Optional<Device> addTaskToDevice(String description, Long deviceId) {
        Optional<Device> deviceOptional = deviceRepository.findById(deviceId);
        if (deviceOptional.isPresent()) {
            Device device = deviceOptional.get();

            Task task = new Task();

            task.setDescription(description);


            ////////////////////////////////////////////////////
            task.setDevice(device);
//            ///////////////////////////////////////////////////

            task = taskRespository.save(task);

            device.getTaskList().add(task);

            device = deviceRepository.save(device);

            return Optional.of(device);
        }
        return Optional.empty();
    }


    public List<Task> getTaskByDevice(Long id) {
        Optional<Device> deviceOptional = deviceRepository.findById(id);
        if (deviceOptional.isPresent()) {
            List<Task> deviceList = deviceOptional.get().getTaskList();
            return deviceList;
        }
        return new ArrayList<>();
    }

    //////////////////////////////////////////////////
    public List<Task> getTaskByClient(Long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        if(optionalClient.isPresent()){
        List<Device> deviceList = optionalClient.get().getDeviceList();

        return taskRespository.findAllByDeviceIn(deviceList);
        }
        return new ArrayList<>();
    }
    ///////////////////////////////////////////////////

    public Optional<Task> modifyTaskDto(ModifyTaskDto dto) {
        return modifyTaskDto(dto.getDescription(), dto.getTaskID(), dto.getDone());
    }

    private Optional<Task> modifyTaskDto(String description, Long taskID, String done) {
        Optional<Task> taskOptional = taskRespository.findById(taskID);
        if(taskOptional.isPresent()){
            Task task = taskOptional.get();

            if(done.equals("true")) {
                task.setDescription("DONE: " + description);
                task.setEnd(LocalDateTime.now());
            }
            if(done.equals("false")){
                task.setDescription("UNDONE: " + description);
                task.setEnd(LocalDateTime.now());
            }

            if(done.equals("modify")){
                task.setDescription(description);
            }



            task = taskRespository.save(task);
            return Optional.of(task);
        }
        return Optional.empty();
    }

    public boolean deleteTask(Long id) {
        if(taskRespository.existsById(id)){
            taskRespository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<Task> getAllTask() {
        return taskRespository.findAll();
    }

    public Optional<Task> modifyTaskAsAdmin(AdminModifyTaskDto dto) {
        return modifyTaskAsAdmin(dto.getDescription(), dto.getEnd(), dto.getTaskId(), dto.isDone());
    }

    private Optional<Task> modifyTaskAsAdmin(String description, LocalDateTime end, Long taskID, boolean done) {
        Optional<Task> taskOptional = taskRespository.findById(taskID);
        if(taskOptional.isPresent()){
            Task task = taskOptional.get();

            if(done) {
                task.setDescription("DONE: " + description);
                task.setEnd(LocalDateTime.now());
            }else{
                task.setDescription("UNDONE: " + description);
                task.setEnd(LocalDateTime.now());
            }

            task = taskRespository.save(task);
            return Optional.of(task);
        }
        return Optional.empty();
    }
}
