package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AddDeviceToClientDto;
import pl.sda.computerservice.model.dto.ModifyDeviceDto;
import pl.sda.computerservice.repository.ClientRepository;
import pl.sda.computerservice.repository.DeviceRepository;
import pl.sda.computerservice.repository.TaskRespository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private TaskRespository taskRespository;

    @Autowired
    private ClientRepository clientRepository;

    public Optional<Client> addDeviceToClient(AddDeviceToClientDto dto) {
        return addDeviceToClient(dto.getName(), dto.getWorth(), dto.getClientId());
    }

    private Optional<Client> addDeviceToClient(String name, double worth, Long clientId) {
        Optional<Client> clientOptional = clientRepository.findById(clientId);
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();

            Device device = new Device();

            device.setName(name);
            device.setWorth(worth);
            device.setClient(client);

            device = deviceRepository.save(device);

            client.getDeviceList().add(device);

            client = clientRepository.save(client);

            return Optional.of(client);
        }
        return Optional.empty();
    }

    public Optional<Device> modifyDevice(ModifyDeviceDto dto) {
        Optional<Device> deviceOptional = deviceRepository.findById(dto.getDeviceId());
        if (deviceOptional.isPresent()) {
            Device device = deviceOptional.get();

            if (dto.getName() != null) {
                device.setName(dto.getName());
            }

            if (String.valueOf(dto.getWorth()) != null) {
                device.setWorth(Double.parseDouble(String.valueOf(dto.getWorth())));
            }

            device = deviceRepository.save(device);
            return Optional.of(device);
        }
        return Optional.empty();
    }

    public List<Task> getDevicesByClient(Long id) {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if(clientOptional.isPresent()){
            List<Device> deviceList = clientOptional.get().getDeviceList();


            return taskRespository.findAllByDeviceIn(deviceList);
        }
        return new ArrayList<>();
    }

    public boolean deleteDevice(Long id) {
        if(deviceRepository.existsById(id)){
            deviceRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<Device> findALl() {
        return deviceRepository.findAll();
    }

    public Optional<Device> findDeviceById(Long deviceId) {
        return deviceRepository.findById(deviceId);
    }
}
