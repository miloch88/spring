package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.computerservice.model.Archive;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.dto.ArchiveTaskDto;
import pl.sda.computerservice.repository.ArchiveRepository;
import pl.sda.computerservice.repository.DeviceRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
@Transactional
public class ArchiveService {

    @Autowired
    private ArchiveRepository archiveRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private TaskService taskService;

    public Optional<Device> saveToArchive(ArchiveTaskDto dto) {
        return saveToArchive(
                dto.getTaskIdArchive(),
//                dto.getBeginingArchive(),
//                dto.getEndArchive(),
                dto.getDescriptionArchive(),
                dto.getDeviceIdArchive());
    }

    private Optional<Device> saveToArchive(Long taskIdArchive,
//                                           LocalDateTime beginingArchive,
//                                           LocalDateTime endArchive,
                                           String descriptionArchive,
                                           Long deviceIdArchive) {

        Optional<Device> deviceOptional = deviceService.findDeviceById(deviceIdArchive);
        if(deviceOptional.isPresent()){
            Device device = deviceOptional.get();

            Archive archive = new Archive();

            archive.setTaskIdArchive(taskIdArchive);
            archive.setDescriptionArchive(descriptionArchive);
//            archive.setBeginingArchive(LocalDateTime.now());
//            archive.setEndArchive(LocalDateTime.now());

            archive.setDevice(device);

            archive = archiveRepository.save(archive);

            device.getArchiveList().add(archive);
            device = deviceRepository.save(device);

            taskService.deleteTask(taskIdArchive);

            return Optional.of(device);
        }
        return Optional.empty();
    }


}
