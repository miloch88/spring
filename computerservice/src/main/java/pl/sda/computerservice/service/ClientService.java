package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.dto.CreateClientDto;
import pl.sda.computerservice.model.dto.FilterClientDto;
import pl.sda.computerservice.model.dto.ModifyClientDto;
import pl.sda.computerservice.repository.ClientRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Optional<Client> addClient(CreateClientDto dto) {

        if (dto.getPassword().equals(dto.getPasswordConfirm()) &&
                !clientRepository.findByEmail(dto.getEmail()).isPresent()) {

            Client createdUser = new Client();

            createdUser.setEmail(dto.getEmail());
            createdUser.setName(dto.getName());
            createdUser.setSurname(dto.getSurname());
            createdUser.setPassword(encoder.encode(dto.getPassword()));

            createdUser = clientRepository.save(createdUser);

            return Optional.of(createdUser);
        }
        return Optional.empty();
    }

    public Optional<Client> modifyClient(ModifyClientDto dto) {
        Optional<Client> clientOptional = clientRepository.findById(dto.getModifyId());
        if (clientOptional.isPresent()) {

            Client client = clientOptional.get();

            if (dto.getName() != null) {
                client.setName(dto.getName());
            }

            if (dto.getSurname() != null) {
                client.setSurname(dto.getSurname());
            }

            if (dto.getPassword() != null && dto.getPassword().equals(dto.getPasswordConfirm())) {
                client.setPassword(dto.getPassword());
            }

            if (dto.getEmail() != null) {
                client.setEmail(dto.getEmail());
            }

            client = clientRepository.save(client);

            return Optional.of(client);
        }
        return Optional.empty();
    }

    public List<Client> filterClient(FilterClientDto dto) {
        List<Client> clientList = clientRepository.findAll();

        return clientList.stream()
                .filter(client -> client.getName().equals(dto.getName())
                        || client.getSurname().equals(dto.getSurname())
                        || client.getId() == dto.getFilterId()
                        || client.getEmail().equals(dto.getEmail()))
                .collect(Collectors.toList());
    }

    public Optional<Client> getUserById(Long id) {
        return clientRepository.findById(id);
    }

    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    public Optional<Client> findByEmail(String email) {
        return clientRepository.findByEmail(email);
    }
}
