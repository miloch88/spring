package pl.sda.computerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.computerservice.model.Archive;

public interface ArchiveRepository extends JpaRepository<Archive, Long> {
}
