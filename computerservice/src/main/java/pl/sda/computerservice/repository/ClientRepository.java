package pl.sda.computerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.computerservice.model.Client;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByEmail(String email);
}
