package pl.sda.computerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;

import java.util.List;

public interface TaskRespository extends JpaRepository<Task, Long> {
    List<Task> findAllByDeviceIn(List<Device> deviceList);
}
