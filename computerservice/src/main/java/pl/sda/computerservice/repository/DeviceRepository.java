package pl.sda.computerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.computerservice.model.Device;

public interface DeviceRepository extends JpaRepository<Device, Long> {
}
