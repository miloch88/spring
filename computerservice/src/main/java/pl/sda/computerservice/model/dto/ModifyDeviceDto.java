package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class ModifyDeviceDto {

    @NotNull
    private Long deviceId;

    private String name;
    @Min(0)
    private double worth;

}
