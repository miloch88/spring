package pl.sda.computerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Archive {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long taskIdArchive;

//    private LocalDateTime beginingArchive;
//    private LocalDateTime endArchive;

    private String descriptionArchive;

    @ManyToOne
    private Device device;


}
