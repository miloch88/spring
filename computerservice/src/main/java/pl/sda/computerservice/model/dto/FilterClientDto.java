package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FilterClientDto {

    private Long filterId;

    private String name;
    private String surname;

    private String email;
}
