package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class AdminModifyTaskDto {

    private boolean done;

    @NotNull
    private Long taskId;

    @NotEmpty
    private String description;

    private LocalDateTime end;
}
