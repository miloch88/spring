package pl.sda.computerservice.model.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class ModifyClientDto {

    @NotNull
    private Long modifyId;

    private String name;
    private String surname;

    @Size(min = 3)
    private String password;
    private String passwordConfirm;

    @Email
    private String email;
}
