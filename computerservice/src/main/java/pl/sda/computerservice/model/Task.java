package pl.sda.computerservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime begining;
    private LocalDateTime end;

    private String description;

    @ManyToOne
    private Device device;

    public Task() {
        this.begining = LocalDateTime.now();
    }
}
