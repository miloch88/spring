package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class ModifyTaskDto {

    private  String done;

    @NotNull
    private Long taskID;

    @NotEmpty
    private String description;

    private LocalDateTime end;

    public ModifyTaskDto() {
    }
}
