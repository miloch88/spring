package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class CreateClientDto {

    @NotEmpty
    private String name;
    @NotEmpty
    private String surname;

    @NotEmpty
    @Size(min = 3)
    private String password;
    private String passwordConfirm;

    @NotEmpty
    @Email
    private String email;
}
