package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddDeviceToClientDto {

    @NotEmpty
    private String name;

    @NotNull
    @Min(0)
    private double worth;

    @NotNull
    private Long clientId;


}
