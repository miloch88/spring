package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AddDeviceToClientDto;
import pl.sda.computerservice.model.dto.ModifyDeviceDto;
import pl.sda.computerservice.service.DeviceService;
import pl.sda.computerservice.service.LoginService;
import pl.sda.computerservice.service.TaskService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/device")
public class DeviceViewController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String add(Model model){
        Optional<Client> optionalClient = loginService.getLoggedInUser();
        if(!optionalClient.isPresent()){
            return "redirect:/login";
        }

        AddDeviceToClientDto addDeviceToClientDto = new AddDeviceToClientDto();
        addDeviceToClientDto.setClientId(optionalClient.get().getId());

        model.addAttribute("add_device", addDeviceToClientDto);
        return "device/add_form";
    }

    @PostMapping("/add")
    public String add(Model model, AddDeviceToClientDto dto){

        ///////////////////////////////////////////////////
        //Tu coś ma być
        ///////////////////////////////////////////////////

        Optional<Client> clientOptional = deviceService.addDeviceToClient(dto);
        model.addAttribute("added_device", dto);

        return "redirect:/view/client/profile/";
    }

    @GetMapping("/profile/{device_id}")
    public String viweDevice(Model model, @PathVariable(name="device_id") Long id){

        List<Task> taskList = taskService.getTaskByDevice(id);

        model.addAttribute("taskList", taskList);

        return "device/profile";
    }

    @GetMapping("/delete")
    public String deleteDevice(Model model, @RequestParam(name="id") Long id){

        deviceService.deleteDevice(id);

        return "redirect:/view/client/profile/";
    }

    @GetMapping("/modify")
    public String modify(Model model, @RequestParam(name = "deviceId") Long id){

        Optional<Client> optionalClient = loginService.getLoggedInUser();
        if(!optionalClient.isPresent()){
            return "redirect:/login";
        }

        ModifyDeviceDto modifyDeviceDto = new ModifyDeviceDto();
        modifyDeviceDto.setDeviceId(id);

        model.addAttribute("modify_device", modifyDeviceDto);
        return "device/modify";
    }

    @PostMapping("/modify")
    public  String modify(Model model, ModifyDeviceDto modifyDeviceDto){

        Optional<Device> deviceOptional = deviceService.modifyDevice(modifyDeviceDto);
        if(deviceOptional.isPresent()){
            return "redirect:/view/client/profile/";
        }
        return "redirect:/view/client/profile/";
    }

}
