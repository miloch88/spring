package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.Archive;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AdminModifyTaskDto;
import pl.sda.computerservice.model.dto.ArchiveTaskDto;
import pl.sda.computerservice.repository.TaskRespository;
import pl.sda.computerservice.service.ArchiveService;
import pl.sda.computerservice.service.TaskService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/admin")
public class AdminViewController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ArchiveService archiveService;

    @Autowired
    private TaskRespository taskRespository;

    @GetMapping("/task")
    public String getTask(Model model) {

        List<Task> list = taskService.getAllTask();
//        List<Task> closedList = new ArrayList<>();
//        List<Task> pendingList = new ArrayList<>();

        model.addAttribute("task", list);
//        model.addAttribute("closedList", closedList);
//        model.addAttribute("pendingList", pendingList);
        model.addAttribute("choices", new ArrayList<>(Arrays.asList(true, false)));

        return "admin/task";
    }

    @GetMapping("/modify")
    public String modify(Model model, @RequestParam(name = "taskId") Long taskId) {

        AdminModifyTaskDto adminModifyTaskDto = new AdminModifyTaskDto();
        adminModifyTaskDto.setTaskId(taskId);

        model.addAttribute("admin", adminModifyTaskDto);
        model.addAttribute("choices", new ArrayList<>(Arrays.asList(true, false)));
        return "admin/modify";
    }

    @PostMapping("/modify")
    public String modify(AdminModifyTaskDto adminModifyTaskDto) {

        Optional<Task> taskOptional = taskService.modifyTaskAsAdmin(adminModifyTaskDto);
        if (taskOptional.isPresent()) {
            return "redirect:/view/admin/task";
        }
        return "redirect:/view/admin/task";
    }

    @GetMapping("/archive")
    public String archive(Model model, @RequestParam(name = "taskId") Long taskId) {

        ArchiveTaskDto archiveTaskDto = new ArchiveTaskDto();
        Task task = taskRespository.findById(taskId).get();

        archiveTaskDto.setTaskIdArchive(task.getId());
        archiveTaskDto.setDeviceIdArchive(task.getDevice().getId());
//        archiveTaskDto.setBeginingArchive(LocalDateTime.now());
//        archiveTaskDto.setEndArchive(LocalDateTime.now());
        archiveTaskDto.setDescriptionArchive(task.getDescription());

        model.addAttribute("archive", archiveTaskDto);

        return "admin/archive";
    }

    @PostMapping("/archive")
    public String archive(ArchiveTaskDto dto) {

        Optional<Device> archivesOptional = archiveService.saveToArchive(dto);
        if (archivesOptional.isPresent()) {
            return "redirect:/view/admin/task";
        }
        return "redirect:/view/admin/task";
    }


}
