package pl.sda.computerservice.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AddDeviceToClientDto;
import pl.sda.computerservice.model.dto.ModifyDeviceDto;
import pl.sda.computerservice.service.DeviceService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/device/")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

//     1. pozwala dodawać sprzęt podanemu użytkownikow
    @PostMapping("/add")
    public ResponseEntity addDeviceToClient(@Valid @RequestBody AddDeviceToClientDto dto){
        Optional<Client> clientOptional = deviceService.addDeviceToClient(dto);
        if(clientOptional.isPresent()){
            return ResponseEntity.ok(clientOptional);
        }
        return ResponseEntity.badRequest().build();
    }

//    2. pozwala modyfikować parametry sprzętu

    @PostMapping("/modify")
    public ResponseEntity modifyDevice(@Valid @RequestBody ModifyDeviceDto dto){
        Optional<Device> deviceOptional = deviceService.modifyDevice(dto);
        if(deviceOptional.isPresent()){
            return ResponseEntity.ok(deviceOptional);
        }
        return ResponseEntity.badRequest().build();
    }

//     3. pozwala czytać sprzęty podanego użytkownika

    @GetMapping("/get")
    public ResponseEntity getDevicesByClient(@RequestParam(name="id") Long id){
        List<Task> deviceList = deviceService.getDevicesByClient(id);
        return ResponseEntity.ok(deviceList);
    }

//    Dodaj w kontrolerze sprzętu metodę usuwania, która usuwa sprzęt wraz ze wszystkimi jego zleceniami!

    @GetMapping("/delete")
    public ResponseEntity deleteDevice(@RequestParam(name = "id") Long id){
        boolean success = deviceService.deleteDevice(id);
        if(success){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
}
