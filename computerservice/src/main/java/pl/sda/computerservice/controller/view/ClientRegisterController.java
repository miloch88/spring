package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.dto.CreateClientDto;
import pl.sda.computerservice.service.ClientService;
import pl.sda.computerservice.service.LoginService;

import java.util.Optional;

@Controller
public class ClientRegisterController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/login")
    public String getLoginViwe(){
        return "client/login_form";
    }

    @GetMapping("/")
    public String getIndex(Model model){
        Optional<Client> clientOptional = loginService.getLoggedInUser();
        if(clientOptional.isPresent()){
            Client client = clientOptional.get();

            model.addAttribute("username", client.getEmail());
        }

        return "index";
    }

    @GetMapping("/register")
    public String getAddView(Model model) {
        model.addAttribute("added_object", new CreateClientDto());
        return "client/register_form";
    }

    @PostMapping("/register")
    public String getAddView(Model model, CreateClientDto dto) {

        if (dto.getPassword().isEmpty() || dto.getPassword().length() < 5) {
            model.addAttribute("added_object", dto);
            model.addAttribute("error_message", "Too simple password");
            return "client/register_form";
        }

        Optional<Client> clientOptional = clientService.addClient(dto);
        if (!clientOptional.isPresent()) {
            model.addAttribute("added_object", dto);
            model.addAttribute("error_message", "User with that email already exists.");
            return "client/register_form";
        }
        return "redirect:/";
    }
}
