package pl.sda.computerservice.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.dto.CreateClientDto;
import pl.sda.computerservice.model.dto.FilterClientDto;
import pl.sda.computerservice.model.dto.ModifyClientDto;
import pl.sda.computerservice.service.ClientService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/client/")
public class ClientController {

    @Autowired
    private ClientService clientService;

//    1. rejestracja użytkownika
    @PostMapping("/add")
    public ResponseEntity addClient(@Valid @RequestBody CreateClientDto dto) {
        Optional<Client> clientOptional = clientService.addClient(dto);

        if (clientOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.badRequest().build();

    }

//    2. modyfikowanie pól użytkownika
    @PostMapping("/modify")
    public ResponseEntity modifyClient(@Valid @RequestBody ModifyClientDto dto) {
        Optional<Client> clientOptional = clientService.modifyClient(dto);
        if (clientOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(clientOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

//    3. sprawdzanie czy użytkownik istnieje
    @PostMapping("/filter")
    public ResponseEntity filterClient(@RequestBody FilterClientDto dto){
        List<Client> clientList = clientService.filterClient(dto);
        //Zwracamy ilość elementów listę
        return ResponseEntity.ok(clientList);
    }

//     4. sprawdzanie danych użytkownika (zwrócenie użytkownika po zapytaniu o użytkownika id)

    @GetMapping("/get/{id}")
    public ResponseEntity getClietnById(@Valid @PathVariable(value = "id") Long id){
        Optional<Client> clientOptional = clientService.getUserById(id);
        if(clientOptional.isPresent()){
            return ResponseEntity.ok(clientOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/getClients")
    public ResponseEntity getAllClients(){
        List<Client> clientList = clientService.getAllClients();
        return ResponseEntity.ok(clientList);
    }
}
