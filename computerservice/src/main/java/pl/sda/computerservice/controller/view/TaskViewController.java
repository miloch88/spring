package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AddDeviceToClientDto;
import pl.sda.computerservice.model.dto.AddTaskToDeviceDto;
import pl.sda.computerservice.service.ClientService;
import pl.sda.computerservice.service.DeviceService;
import pl.sda.computerservice.service.LoginService;
import pl.sda.computerservice.service.TaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/task")
public class TaskViewController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private DeviceService deviceService;

    @GetMapping("/add")
    public String getAddTask(Model model, @RequestParam(name = "deviceId") Long deviceId) {

        Optional<Client> optionalClient = loginService.getLoggedInUser();
        if (!optionalClient.isPresent()) {
            return "redirect:/login";
        }

        AddTaskToDeviceDto addTaskToDeviceDto = new AddTaskToDeviceDto();
        addTaskToDeviceDto.setDeviceId(deviceId);

        model.addAttribute("added_task", addTaskToDeviceDto);

        return "task/task_form";
    }

    @PostMapping("/add")
    public String addTask(Model model, AddTaskToDeviceDto dto) {

        Optional<Device> deviceOptional = taskService.addTaskToDevice(dto);
        if (deviceOptional.isPresent()) {
            return "redirect:/view/client/profile/";
        }

        return "redirect:/view/client/profile/";
    }

    @GetMapping("/addNew")
    public String addNewTask(Model model) {

        Optional<Client> clientOptional = loginService.getLoggedInUser();
        if (clientOptional.isPresent()) {
            List<Device> deviceList = clientOptional.get().getDeviceList();

            model.addAttribute("newTask", new AddTaskToDeviceDto());
            model.addAttribute("deviceList", deviceList);
            return "task/newTaskForm";
        }
        return "redirect:/lgoin";
    }

    @PostMapping("/addNew")
    public String addNewTaskPost(Model model, AddTaskToDeviceDto dto) {

        Optional<Device> deviceOptional = taskService.addTaskToDevice(dto);
        if (deviceOptional.isPresent()) {
            return "redirect:view/task/allTask";
        }
        return "redirect:view/task/allTask";
    }

    @GetMapping("/list")
    public String getList(Model model, @RequestParam(name = "deviceId") Long deviceId) {

        Optional<Device> deviceOptional = deviceService.findDeviceById(deviceId);
        if (deviceOptional.isPresent()) {
            Device device = deviceOptional.get();
            List<Task> taskList = taskService.getTaskByDevice(deviceId);

            model.addAttribute("device", device);
            model.addAttribute("taskList", taskList);

            return "device/task";
        }

        return "redirect:/login";
    }

    @GetMapping("/allTask")
    public String getAllList(Model model) {

        Optional<Client> clientOptional = loginService.getLoggedInUser();
        if (!clientOptional.isPresent()) {
            return "redirect:/login";
        }

        Client client = clientOptional.get();
        Long id = clientOptional.get().getId();
        List<Task> taskList = taskService.getTaskByClient(id);

        model.addAttribute("tasks", taskList);
        model.addAttribute("client", client);

        return "client/task";
    }


}
