package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.Client;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.service.ClientService;
import pl.sda.computerservice.service.LoginService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/client")
public class ClientViewController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private LoginService loginService;

    //Dodanie użytkownika jest przeniesione do ClientRegisterController

    @GetMapping("/profile/")
    public String viewProfile(Model model){

        Optional<Client> optionalClient = loginService.getLoggedInUser();

        if(optionalClient.isPresent()){
            Client client = optionalClient.get();
            List<Device> deviceList = client.getDeviceList();

            model.addAttribute("client", client);
            model.addAttribute("deviceList", deviceList);
            return "client/profile";
        }
        return "redirect:/login";
    }

}
