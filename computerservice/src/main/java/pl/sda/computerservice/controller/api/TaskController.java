package pl.sda.computerservice.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.Device;
import pl.sda.computerservice.model.Task;
import pl.sda.computerservice.model.dto.AddTaskToDeviceDto;
import pl.sda.computerservice.model.dto.ModifyTaskDto;
import pl.sda.computerservice.service.TaskService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/task/")
public class TaskController {

    @Autowired
    private TaskService taskService;

//    1. pozwala dodawać zlecenie do podanego sprzętu

    @PostMapping("/add")
    public ResponseEntity addTaskToDevice(@Valid @RequestBody AddTaskToDeviceDto dto) {
        Optional<Device> deviceOptional = taskService.addTaskToDevice(dto);
        if (deviceOptional.isPresent()) {
            return ResponseEntity.ok(deviceOptional);
        }
        return ResponseEntity.badRequest().build();
    }

//    2. pozwala sprawdzać(listować) zlecenia podanego sprzętu

    @GetMapping("/get")
    public ResponseEntity getTaskByDevice(@RequestParam(name = "id") Long id) {
        List<Task> taskList = taskService.getTaskByDevice(id);
        return ResponseEntity.ok(taskList);
    }

//     3. pozwala sprawdzać(listować) zlecenia podanego użytkownika

    @GetMapping("/get/{id}")
    public ResponseEntity getTaskByClient(@PathVariable(name = "id") Long id) {
        List<Task> taskList = taskService.getTaskByClient(id);
        return ResponseEntity.ok(taskList);
    }

//    4. pozwala modyfikować zlecenie (oznaczać je jako zrobione/ dodawać datę zrealizowania zlecenia)
// [kilka metod - metoda 'done' - zlecenie wykonane, metoda 'undone'
// - zlecenie zostało wadliwie naprawione (usuwamy datę zakończenia), 'modyfikacja opisu usterki' ]


    @PostMapping("/modify")
    public ResponseEntity modifyTask(@Valid @RequestBody ModifyTaskDto dto){
        Optional<Task> taskOptional = taskService.modifyTaskDto(dto);
        if (taskOptional.isPresent()) {
            return ResponseEntity.ok(taskOptional);
        }
        return ResponseEntity.badRequest().build();
    }

//    5. pozwala usuwać zlecenie

    @DeleteMapping("/delete")
    public ResponseEntity deleteTask(@RequestParam(name="id")Long id){
        boolean success = taskService.deleteTask(id);
        if(success){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
}
