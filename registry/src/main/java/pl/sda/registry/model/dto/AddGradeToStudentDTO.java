package pl.sda.registry.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddGradeToStudentDTO {

    @NotNull
    private  AddGradeDto gradeDto;

    @NotNull
    private Long userId;
}
