package pl.sda.registry.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserStatsDto {

    private String name;
    private String surname;
    private List<SubjectGradesDto> grades;
    private Double mean;

//    {
//            "name": "",
//            "surname": "",
//            "grades": [
//        {
//            "subject": "",
//                "grades": [],
//            "mean": 1.0
//        },
//        {
//            "subject": "",
//                "grades": [],
//            "mean": 1.0
//        },
//        {
//            "subject": "",
//                "grades": [],
//            "mean": 1.0
//        }
//    ],
//        "mean": 1.0
//    }

}
