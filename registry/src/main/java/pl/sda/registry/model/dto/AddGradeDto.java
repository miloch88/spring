package pl.sda.registry.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;
import pl.sda.registry.model.Subject;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddGradeDto {

    //@NotNull tzn opcjolane
//    @Min(1)
//    @Max(6)
    @NotNull
    @Range(min = 1, max =6)
    private double grade;
    @NotNull
    private Subject subject;

}
