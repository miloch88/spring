package pl.sda.registry.model;

public enum Subject {
    LANG_PL,
    LANG_EN,
    MATH,
    IT
}
