package pl.sda.registry.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateAppUsetDto {
    private String username;
    private String password;

    private String passwordConfirm;
}
