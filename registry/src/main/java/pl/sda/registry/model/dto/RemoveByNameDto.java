package pl.sda.registry.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class RemoveByNameDto {

    @NotNull
    private String name;
    @NotNull
    private String surname;

}
