package pl.sda.registry.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data //gettery, settery i constukotr, toString, Equals & HashCode
@NoArgsConstructor
public class UpdateUserDto {

    @NotNull
    private Long updatedUserId;

    private String name;
    private String surname;

    @Size(min=4, max = 255)
    private String password;
}
