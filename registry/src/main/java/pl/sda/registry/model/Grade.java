package pl.sda.registry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.registry.model.dto.AddGradeDto;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@AllArgsConstructor
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long id;
    private double grade;
    private Subject subject;
    private LocalDateTime dateAdded;

    public Grade(){
        dateAdded = LocalDateTime.now();
    }

    public Grade(Long id, double grade, Subject subject){
        this.id = id;
        this.grade = grade;
        this.subject = subject;
        this.dateAdded = LocalDateTime.now();
    }

    public static Grade createFromDto(AddGradeDto dto){
        Grade grade = new Grade();
        grade.setGrade(dto.getGrade());
        grade.setSubject(dto.getSubject());

        return grade;
    }
}
