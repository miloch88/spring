package pl.sda.registry.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.registry.model.dto.CreateAppUsetDto;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String password;

    private String name;
    private String surname;

//  1. Zmodyfikuj model AppUser i Grade. Dodaj relację między nimi.
//  AppUser powinien mieć relację jeden do wielu do ocen.

//    lazy - nie ładuje danych dopóki nie są potrzebne
//    cascade- persist sprawi, że dane w liście zostaną zapisane do bazy przy zapisie usera
    @OneToMany(fetch =  FetchType.LAZY)
    private List<Grade> gradeList;

    public static AppUser createFromDto(CreateAppUsetDto appUsetDto){
        AppUser appUser = new AppUser();
        appUser.setUsername(appUsetDto.getUsername());
        appUser.setPassword(appUsetDto.getPassword());

        return appUser;
    }

}
