package pl.sda.registry.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.sda.registry.model.AppUser;

import java.util.List;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long>, JpaSpecificationExecutor<AppUser> {

    List<AppUser> findAllBySurname(String surname);

    int deleteByNameAndSurname(String name, String surname);

    List<AppUser> findAllByNameIsLikeAndSurnameIsLike(String name, String surname);
    List<AppUser> findAllByNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(String name, String surname);
 }
