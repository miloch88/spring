package pl.sda.registry.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.registry.model.Grade;

import java.util.List;

@Repository
public interface GradeRspository extends JpaRepository<Grade, Long> {

    List<Grade> findAllByGradeGreaterThan(double grade);

}
