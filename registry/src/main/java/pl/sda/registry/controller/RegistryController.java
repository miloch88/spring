package pl.sda.registry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeToStudentDTO;
import pl.sda.registry.model.dto.GetUSerGradesBySubjectDto;
import pl.sda.registry.model.dto.UserStatsDto;
import pl.sda.registry.service.RegistryService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

//    2. Dodaj kontroller, nazwij go RegistryController.
//    Ten kontroler ma umożliwiać oddawanie ocen poszczególnym studentom/użytkownikom.

@RestController
@RequestMapping("/registry")
public class RegistryController {

    @Autowired
    private RegistryService registryService;

//    3. Dodaj metodę do dodawania oceny z podanego przedmiotu podanemu studentowi (parametry: id_user, subject, grade)
//    a. zrealizuj to zapytanie z RequestBody (DTO)

    @PostMapping("/addgrade")
    public ResponseEntity addGradeToStudent(@RequestBody AddGradeToStudentDTO dto){
        Optional<AppUser> optionalAppUser = registryService.addGrade(dto);
        if(optionalAppUser.isPresent()){
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    //    c. zrealizuj to zapytanie z PathVariable
    @GetMapping("/addgrade/{id}/{grade}/{subject}")
    public ResponseEntity addGradeToStudentPath(@PathVariable(name = "id") Long userId,
                                            @PathVariable(name = "grade") Double grade,
                                            @PathVariable(name = "subject")Subject subject){
        Optional<AppUser> optionalAppUser = registryService.addGrade(userId, grade, subject);
        if(optionalAppUser.isPresent()){
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    //    b. zrealizuj to zapytanie z RequestParam
    @GetMapping("/addgrade")
    public ResponseEntity addGradeToStudentParam(@RequestParam(name = "id") Long userId,
                                            @RequestParam(name = "grade") Double grade,
                                            @RequestParam(name = "subject")Subject subject){
        Optional<AppUser> optionalAppUser = registryService.addGrade(userId, grade, subject);
        if(optionalAppUser.isPresent()){
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

//    4. Dodaj metodę pobierania ocen studenta z podanego przedmiotu (id student, subject)

    @PostMapping("/getUserGrades")
    public ResponseEntity getGradesBySubject(@Valid @RequestBody GetUSerGradesBySubjectDto dto){
        List<Grade> gradeList = registryService.getGradesBySubject(dto);
        return ResponseEntity.ok(gradeList);
    }

//    6. W dowolnej formie (path variable, request body, request param):
//    a.  zrealizuj zapytanie do listowania studentów których średnia jest
//    poniżej pewnego zadanego progu.

    @GetMapping("/getWeekStudent/{mean}")
    public ResponseEntity getGradesBySubject(@PathVariable(name = "mean") double mean){
        List<AppUser> userList = registryService.getWeakStudents(mean);
        return  ResponseEntity.ok(userList);
    }

//    Żeby zrobić podpunkt B należy dodać drugi parametr którym jest data.
//    następnie przekazać go do serwisu jako drugie kryterium filtrowania.

//    7. Dodaj metodę pobierania statusu studenta tj. obiekt zawierający dane studenta,
//    wszystkie jego oceny oraz średnie z przedmiotów a dodatkowo średnia ogólna.
//    Obiekt jest bardzo złożony i powinien zawierać wiele pól. Struktura powinna wyglądać następująco:



    @GetMapping("/getUserStats/{id}")
    public ResponseEntity getStats(@PathVariable(name = "id") Long userId){
        Optional<UserStatsDto> userStatsDto = registryService.getStats(userId);
        if(userStatsDto.isPresent()){
            return ResponseEntity.ok(userStatsDto.get());
        }
        return ResponseEntity.badRequest().build();
    }

}
