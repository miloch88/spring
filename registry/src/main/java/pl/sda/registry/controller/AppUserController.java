package pl.sda.registry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.dto.CreateAppUsetDto;
import pl.sda.registry.model.dto.NameAndSurnameFilterDto;
import pl.sda.registry.model.dto.RemoveByNameDto;
import pl.sda.registry.model.dto.UpdateUserDto;
import pl.sda.registry.service.AppUserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
//request mapping oznacza, że wszstkie metody z tej klasy są dostępne pod adresem /user/
@RequestMapping(path= "/user/")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    //requesr apping oznacza, że adresem uri jest 'add'
    //mappingi składają się na /user/add
//    @RequestMapping(path = "/add", method = RequestMethod.POST)
    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody CreateAppUsetDto user){

        Optional<AppUser> appUserOptional = appUserService.addUser(user);
        if(appUserOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }

        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/get/{id}")
    public ResponseEntity get(@PathVariable(name="id", required = true) Long id){
        Optional<AppUser> appUserOptional = appUserService.getUserById(id);
        if(appUserOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/updatename/{id}/{name}")
    public ResponseEntity updateName(@PathVariable(name = "id", required = true) Long id,
                                     @PathVariable(name = "name", required = true) String name){
        Optional<AppUser> appUserOptional = appUserService.updateName(id, name);
        if(appUserOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/updatesurname/{id}/{surname}")
    public ResponseEntity updateSurname(@PathVariable(name = "id", required = true) Long id,
                                     @PathVariable(name = "surname", required = true) String surname){
        Optional<AppUser> appUserOptional = appUserService.updateSurame(id, surname);
        if(appUserOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/getusers/")
    public ResponseEntity findAllUser(){
        List<AppUser> userList = appUserService.getUserList();
        if(!userList.isEmpty()){
        return ResponseEntity.status(HttpStatus.OK).body(userList);
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/getlistuser/{surname}/")
    public ResponseEntity finsAllUserBySurname(@PathVariable(name="surname", required = true) String surname){
        List<AppUser> userList = appUserService.getUserListBySurname(surname);
        if(!userList.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(userList);
        }
        return ResponseEntity.badRequest().build();
    }

//    Zadanie domowe, zapytanie z DTO

//    1. Stwórz zapytanie z DTO, którego zadaniem jest aktualizować jednocześnie imie, nazwisko
    @PostMapping("/update")
    public ResponseEntity update(@Valid @RequestBody UpdateUserDto updateUserDto){
        Optional<AppUser> optionalAppUser = appUserService.update(updateUserDto);
        if(optionalAppUser.isPresent()){
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

//2. Stwórz zapytanie z DTO, którego zadaniem jest aktualizować jednocześnie imie, nazwisko, password
    //podpięte do zadania pierwszego

//3. Stwórz zapytanie do usuwania użytkownika po id

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") Long id){
        boolean success = appUserService.deleteUser(id);
        if(success){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("delete")
        public ResponseEntity deleteParam(@RequestParam(name= "id") Long id){
            boolean success = appUserService.deleteUser(id);
            if(success){
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.badRequest().build();
    }

//4. Stwórz zapytanie z DTO do usuwania użytkowników z podanym imieniem i nazwiskiem
    @DeleteMapping("/deleteu")
    public ResponseEntity deleteReuestBody(@Valid @RequestBody RemoveByNameDto dto){
        int howManyRemoved = appUserService.deleteUser(dto);
        if(howManyRemoved > 0){
            return ResponseEntity.ok(howManyRemoved);
        }
        return ResponseEntity.badRequest().build();
    }

//5. Stwórz zapytanie z DTO które filtruje użytkowników po imieniu i nazwisku i zwraca ich listę (nie zwracaj uwagi na duże/małe litery).
    @PostMapping("/findByNameAndSurname")
    public ResponseEntity findByNameAndSurname(@Valid @RequestBody NameAndSurnameFilterDto dto){
        List<AppUser> userList = appUserService.getAllUsersByNameAndSurname(dto);
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }
}
