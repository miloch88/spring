package pl.sda.registry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeDto;
import pl.sda.registry.service.GradeService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/grade/")
public class GradeController {

    @Autowired
    private GradeService gradeService;

    @PostMapping("/add")
    public ResponseEntity addGradePost(@Valid @RequestBody AddGradeDto dto) {

        Optional<Grade> gradeOptional = gradeService.addGrade(dto);
        if (gradeOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/add")
    public ResponseEntity addGradeGet(@RequestParam(name = "grade") Double grade,
                                      @RequestParam(name = "subject") Subject subject) {
        Optional<Grade> gradeOptional = gradeService.addGradeByParam(grade, subject);
        if (gradeOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/getgrade")
    public ResponseEntity getGradeList(){
        List<Grade> gradeList = gradeService.getGradeList();
        if(!gradeList.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(gradeList);
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/getgrade/{grade}")
    public ResponseEntity getGradeListUp(@PathVariable(name = "grade", required = true) double grade){
        List<Grade> gradeList = gradeService.getGradeListUp(grade);
        if(!gradeList.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(gradeList);
        }
        return ResponseEntity.badRequest().build();
    }


}


