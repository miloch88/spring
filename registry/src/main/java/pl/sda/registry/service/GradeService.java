package pl.sda.registry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeDto;
import pl.sda.registry.repository.GradeRspository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GradeService {

    @Autowired
    private GradeRspository gradeRspository;

    public Optional<Grade> addGrade(AddGradeDto dto) {

        if (dto.getGrade() > 0 && dto.getGrade() < 7) {
            Grade grade = Grade.createFromDto(dto);
            grade = gradeRspository.save(grade);

            return Optional.of(grade);
        }
        return Optional.empty();
    }

    public Optional<Grade> addGradeByParam(Double gradeNumber, Subject subject) {

        if (gradeNumber > 0 && gradeNumber < 7) {

            Grade grade = new Grade();
            grade.setGrade(gradeNumber);
            grade.setSubject(subject);
            grade = gradeRspository.save(grade);

            return Optional.of(grade);
        }
        return Optional.empty();
    }

    public List<Grade> getGradeList(){
        List<Grade> gradeList = gradeRspository.findAll();
        return gradeList;
    }

    public List<Grade> getGradeListUp(double grade) {
        List<Grade> list = gradeRspository.findAllByGradeGreaterThan(grade);

        return list;
    }
}

