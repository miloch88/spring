package pl.sda.registry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeToStudentDTO;
import pl.sda.registry.model.dto.GetUSerGradesBySubjectDto;
import pl.sda.registry.model.dto.SubjectGradesDto;
import pl.sda.registry.model.dto.UserStatsDto;
import pl.sda.registry.repository.AppUserRepository;
import pl.sda.registry.repository.GradeRspository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class RegistryService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private GradeRspository gradeRspository;

    public void getAllGradesFromStudent(Long studentId) {
        Optional<AppUser> appUserOpt = appUserRepository.findById(studentId);
        AppUser appUser = appUserOpt.get();

        List<Grade> grades = appUser.getGradeList();
    }

    public Optional<AppUser> addGrade(AddGradeToStudentDTO dto) {
//        Zadanie 1
//        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getUserId());
        return addGrade(dto.getUserId(), dto.getGradeDto().getGrade(), dto.getGradeDto().getSubject());
    }

    public Optional<AppUser> addGrade(Long useId, Double gradeX, Subject subject) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(useId);
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();

            Grade grade = new Grade();
//            grade.setGrade(dto.getGradeDto().getGrade());
//            grade.setSubject((dto.getGradeDto().getSubject()));

            grade.setGrade(gradeX);
            grade.setSubject(subject);

            grade = gradeRspository.save(grade);

            appUser.getGradeList().add(grade);

            appUser = appUserRepository.save(appUser);

            return Optional.of(appUser);
        }
        return Optional.empty();
    }

    public List<Grade> getGradesBySubject(GetUSerGradesBySubjectDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getUserID());
        if (optionalAppUser.isPresent()) {
            List<Grade> grades = optionalAppUser.get().getGradeList();

            return grades
                    .stream()
                    .filter(grade -> grade.getSubject() == dto.getSubject())
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<AppUser> getWeakStudents(double mean) {
        List<AppUser> appUsers = appUserRepository.findAll();

        return appUsers.stream().filter(appUser -> {
            return appUser.getGradeList().stream()
                    //todo: filtr daty trafia tutaj
                    .map(grade -> grade.getGrade())
                    .mapToDouble(p -> p).average()
                    .getAsDouble() < mean;
        }).collect(Collectors.toList());
    }

    public Optional<UserStatsDto> getStats(Long userId) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(userId);
        if (optionalAppUser.isPresent()) {
            AppUser user = optionalAppUser.get();

            UserStatsDto userStatsDto = new UserStatsDto();
            userStatsDto.setName(user.getName());
            userStatsDto.setSurname(user.getSurname());

            //Obliczanie średniej wszystkich ocen
            userStatsDto.setMean(user.getGradeList().stream()
                    .map(Grade::getGrade)
//            orElse - jeśli nie będzie ocen zwracamy 0.0
                    .mapToDouble(p -> p).average().orElse(0.0));

            //pobieranie listy wszystkich przedmiotów
            Set<Subject> subjects = user.getGradeList()
                    .stream()
                    .map(Grade::getSubject)
                    .collect(Collectors.toSet());

            List<SubjectGradesDto> subjectGrades = subjects.stream()
                    .map(subject -> {
                        SubjectGradesDto subjectGradesDto = new SubjectGradesDto();
                        subjectGradesDto.setSubject(subject);
                        //do tego miejsca ^^ ustawiam tylko subject i
//                        przygotowuhe obiekt do zwrócenia

                        //pobieranie z użytkownika listy wszystkich ocen
                        subjectGradesDto.setGrades(user.getGradeList()
                                .stream()
                                //Filturję podany przedmiot
                                .filter(grade -> grade.getSubject() == subject)
                                //Wyciągam tylko oceny
                                .map(Grade::getGrade)
                                //Zbieram wszystkie oceny w listę
                                .collect(Collectors.toList()));

                        subjectGradesDto.setMean(subjectGradesDto
                                //Biorę wszystkie oceny z jednego przedmiotu
                                .getGrades()
                                .stream()
                                //mapuje na double
                                .mapToDouble(grade -> grade)
                                //Obliczam średnią
                                .average()
                                //zwraca 0.0 jeśli nie znalało oceny
                                .orElse(0.0));

                        return subjectGradesDto;
                    }).collect(Collectors.toList());

            //ustawiamy grade na stat
            userStatsDto.setGrades(subjectGrades);

            return Optional.of(userStatsDto);
        }
        return Optional.empty();
    }
}
