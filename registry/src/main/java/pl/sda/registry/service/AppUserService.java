package pl.sda.registry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.registry.model.dto.NameAndSurnameFilterDto;
import pl.sda.registry.model.dto.RemoveByNameDto;
import pl.sda.registry.model.dto.UpdateUserDto;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.dto.CreateAppUsetDto;
import pl.sda.registry.repository.AppUserRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public Optional<AppUser> addUser(CreateAppUsetDto dto) {
        if (dto.getPassword().equals(dto.getPasswordConfirm())) {
            AppUser cretedUser = AppUser.createFromDto(dto);

            cretedUser = appUserRepository.save(cretedUser);

            return Optional.of(cretedUser);
        }


        return Optional.empty();
    }

    public Optional<AppUser> getUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public Optional<AppUser> updateName(Long id, String name) {
        Optional<AppUser> optionalAppUser = getUserById(id);
        if (optionalAppUser.isPresent()) {
            AppUser user = optionalAppUser.get();
            user.setName(name);
            user = appUserRepository.save(user);
            return Optional.of(user);
        }
        return Optional.empty();

    }

    public Optional<AppUser> updateSurame(Long id, String surname) {
        Optional<AppUser> optionalAppUser = getUserById(id);
        if (optionalAppUser.isPresent()) {
            AppUser user = optionalAppUser.get();
            user.setSurname(surname);
            user = appUserRepository.save(user);
            return Optional.of(user);
        }
        return Optional.empty();
    }

    public List<AppUser> getUserList() {
        List<AppUser> list = appUserRepository.findAll();
        return list;
    }

    public List<AppUser> getUserListBySurname(String surname) {
        List<AppUser> list = appUserRepository.findAllBySurname(surname);
        return list;
    }

    public Optional<AppUser> update(UpdateUserDto dto) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(dto.getUpdatedUserId());
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();

            if (dto.getName() != null) {
                appUser.setName(dto.getName());
            }
            ;

            if (dto.getSurname() != null) {
                appUser.setSurname(dto.getSurname());
            }

            if (dto.getPassword() != null) {
                appUser.setPassword(dto.getPassword());
            }

            appUser = appUserRepository.save(appUser);

            return Optional.of(appUser);
        }
        return Optional.empty();
    }

    public boolean deleteUser(Long id) {
        if (appUserRepository.existsById(id)) {
            appUserRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public int deleteUser(RemoveByNameDto dto) {
        return appUserRepository.deleteByNameAndSurname(dto.getName(), dto.getSurname());
    }

    public List<AppUser> getAllUsersByNameAndSurname(NameAndSurnameFilterDto dto) {

        return appUserRepository.findAllByNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(dto.getName(), dto.getSurname());
    }
}
