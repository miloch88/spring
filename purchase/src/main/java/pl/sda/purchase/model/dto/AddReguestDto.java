package pl.sda.purchase.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddReguestDto {

    @NotNull
    private Long purchaseId;

    @NotNull
    private Long itemId;

    @NotNull
    private Long quantity;

}
