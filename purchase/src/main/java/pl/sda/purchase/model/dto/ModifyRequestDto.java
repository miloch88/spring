package pl.sda.purchase.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@Setter
@Getter
@NoArgsConstructor
public class ModifyRequestDto {

    @NotNull
    private Long itemId;

    @NotNull
    private Long quantity;

    @NotNull
    private Long requestId;
}
