package pl.sda.purchase.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.purchase.model.Store;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.net.URL;

@Getter
@Setter
@NoArgsConstructor
public class AddItemToStoreDto {

    @NotEmpty
    private String name;

    @NotNull
    private Double price;

    private String url;

    @NotNull
    private Long storeId;

}
