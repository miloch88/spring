package pl.sda.purchase.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class ModifyStoreDto {

    @NotNull
    private Long storeId;

    @NotEmpty
    private String name;
}
