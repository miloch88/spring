package pl.sda.purchase.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class ModifyItemDto {

    @NotNull
    private Long itemId;

    @NotEmpty
    private String name;

    @NotNull
    private Double price;

    @NotEmpty
    private String url;

    @NotNull
    private Long storeId;
}
