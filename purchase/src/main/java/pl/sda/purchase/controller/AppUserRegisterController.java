package pl.sda.purchase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.purchase.model.AppUser;
import pl.sda.purchase.model.dto.RegisterUserDto;
import pl.sda.purchase.service.AppUserService;
import pl.sda.purchase.service.LoginService;

import java.util.Optional;

@Controller
public class AppUserRegisterController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;

    //Główna strona
    @GetMapping("/")
    public String getIndex(Model model){

        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();

        if(optionalAppUser.isPresent()){

            AppUser appUser = optionalAppUser.get();

            model.addAttribute("logged", appUser.getEmail());
        }
        return "index";
    }

    //Logowanie GET
    @GetMapping("/login")
    public String loginView(){
        return "appUser/login_form";
    }


    //Dodawanie użytkownika GET i POST
    @GetMapping("/register")
    public String addUser(Model model){
        model.addAttribute("new_user", new RegisterUserDto());
        return "appUser/register_form";
    }

    @PostMapping("/register")
    public String addUserPost(Model model, RegisterUserDto dto){

        if(dto.getPassword().isEmpty() || dto.getPassword().length() <5){
            model.addAttribute("new_user", dto);
            model.addAttribute("error_message", "Password is too weak");
            return "appUser/register_form";
        }

        Optional<AppUser> optionalAppUser = appUserService.addUser(dto);

        if(!optionalAppUser.isPresent()){
            model.addAttribute("new_user", dto);
            model.addAttribute("error_message", "This email already exists");
            return "appUser/register_form";
        }

        return "redirect:/";
    }
}
