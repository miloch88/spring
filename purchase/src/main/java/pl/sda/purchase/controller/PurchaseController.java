package pl.sda.purchase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.purchase.model.Item;
import pl.sda.purchase.model.Purchase;
import pl.sda.purchase.model.Request;
import pl.sda.purchase.model.dto.AddPurchaseDto;
import pl.sda.purchase.model.dto.ModifyItemDto;
import pl.sda.purchase.service.ItemService;
import pl.sda.purchase.service.LoginService;
import pl.sda.purchase.service.PurchaseService;
import pl.sda.purchase.service.RequestService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/order")
public class PurchaseController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private RequestService requestService;

    @GetMapping("/api/add")
    public String addPurchase(Model model) {

        model.addAttribute("new_purchase", new AddPurchaseDto());

        return "purchase/add_purchase";
    }

    @PostMapping("/api/add")
    public String addPurchasePost(Model Model, AddPurchaseDto dto){

        Optional<Purchase> optionalPurchase = purchaseService.addPurchase(dto);

        return "redirect:/order/get";
    }

    @GetMapping("/get")
    public String getList(Model model){
        List<Purchase> purchasesList = purchaseService.getAll();

        model.addAttribute("purchase_list", purchasesList);
        Long loggedId = loginService.getLoggedInUser().get().getId();
        model.addAttribute("loggedIn", loggedId);
        return "purchase/list";
    }

    @PostMapping("/get")
    public String getListPost(Model model, @RequestParam(name="id") Long id){

        List<Purchase> purchaseList = purchaseService.getAll();
        List<Request> requestList = requestService.getAll();

        model.addAttribute("purchase_list", purchaseList);
        model.addAttribute("request_list", requestList.stream()
        .filter(request -> request.getPurchase().getId() == id).collect(Collectors.toList()));
        model.addAttribute("new_list", purchaseList.stream()
        .filter(purchase -> purchase.getId() == id).collect(Collectors.toList()));
        Long loggedId = loginService.getLoggedInUser().get().getId();
        model.addAttribute("loggedIn", loggedId);

        return "purchase/list";
    }

    @GetMapping("/api/remove")
    public String remove(@RequestParam(name="id") Long id){
        purchaseService.removePurchase(id);

        return "redirect:/order/get";
    }

    @GetMapping("/admin/close")
    public String close(@RequestParam(name = "id") Long id){
        purchaseService.closePurchase(id);

        return "redirect:/order/get";
    }


}
