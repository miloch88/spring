package pl.sda.purchase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.purchase.model.Item;
import pl.sda.purchase.model.Store;
import pl.sda.purchase.model.dto.AddItemToStoreDto;
import pl.sda.purchase.model.dto.ModifyItemDto;
import pl.sda.purchase.service.ItemService;
import pl.sda.purchase.service.StoreService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private StoreService storeService;

    @GetMapping("/api/add")
    public String addItem(Model model){

        List<Store> storeList = storeService.getAllStore();

        model.addAttribute("store_list", storeList);
        model.addAttribute("new_item", new AddItemToStoreDto());

        return "item/add_item";
    }

    @PostMapping("/api/add")
    public String addItemPost(Model model, AddItemToStoreDto dto){

        if(dto.getName().isEmpty()){
            model.addAttribute("new_item", dto);
            model.addAttribute("error_message", "Name cannot be empty");
            return "item/add_item";
        }

        Optional<Store> optionalStore  = itemService.addItemToStore(dto);
        if(optionalStore.isPresent()){
            return "redirect:/item/get";
        }
        return "redirect:/item/get";
    }

    @GetMapping("/get")
    public String getList(Model model){
        List<Item> itemList = itemService.findAll();

        model.addAttribute("item_list", itemList);

        return "item/list";
    }

    @GetMapping("/admin/modify")
    public String modify(Model model, @RequestParam(name="id") Long id){
        Optional<Item> optionalItem = itemService.getById(id);

        if(!optionalItem.isPresent()){
            return "redirect:/item/get";
        }

        ModifyItemDto dto = new ModifyItemDto();
        dto.setItemId(id);

        List<Store> storeList = storeService.getAllStore();

        model.addAttribute("store_list", storeList);
        model.addAttribute("modify_item", dto);

        return "item/modify";
    }

    @PostMapping("/admin/modify")
    public String modify(ModifyItemDto dto){
        Optional<Item> optionalItem = itemService.modifyItem(dto);
        if(optionalItem.isPresent()){
            return "redirect:/item/get";
        }
        return "redirect:/item/get";
    }

    @GetMapping("/admin/remove")
    public String remove(@RequestParam(name="id") Long id){
        itemService.removeItem(id);

        return "redirect:/item/get";
    }

}
