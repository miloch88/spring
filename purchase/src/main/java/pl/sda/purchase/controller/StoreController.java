package pl.sda.purchase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.purchase.model.Store;
import pl.sda.purchase.model.dto.AddStoreDto;
import pl.sda.purchase.model.dto.ModifyStoreDto;
import pl.sda.purchase.service.StoreService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/store")
public class StoreController {

    @Autowired
    public StoreService storeService;

    @GetMapping("/add")
    public String addStore(Model model) {
        model.addAttribute("new_store", new AddStoreDto());
        return "store/add_store";
    }

    @PostMapping("/add")
    public String addStorePost(Model model, AddStoreDto dto) {

        if (dto.getName().isEmpty()) {
            model.addAttribute("new_store", dto);
            model.addAttribute("error_message", "Name is empty");
            return "store/add_store";
        }

        Optional<Store> optionalStore = storeService.addStore(dto);

        if (!optionalStore.isPresent()) {
            model.addAttribute("new_store", dto);
            model.addAttribute("error_message", "This store already exists");
            return "store/add_store";
        }
        return "redirect:/api/store/get";
    }

    @GetMapping("/get")
    public String getList(Model model){

        List<Store> storeList = storeService.getAllStore();
        model.addAttribute("store_list", storeList);

        return "store/list";
    }

    @GetMapping("/remove")
    public String remove(@RequestParam(name="id") Long id){
        storeService.removeStore(id);

        return "redirect:/api/store/get";
    }

    @GetMapping("/modify")
    public String modify(Model model, @RequestParam(name="id") Long id){
        Optional<Store> optionalStore = storeService.getById(id);
        if(!optionalStore.isPresent()){
            return "redirect:/api/store/get";
        }

        ModifyStoreDto dto = new ModifyStoreDto();
        dto.setStoreId(id);

        model.addAttribute("modify_store", dto);

        return "store/modify";
    }

    @PostMapping("/modify")
    public String modify(ModifyStoreDto dto){
        Optional<Store> optionalStore = storeService.modifyStore(dto);
        if(optionalStore.isPresent()){
            return "redirect:/api/store/get";
        }
        return "redirect:/api/store/get";
    }
}
