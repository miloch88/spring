package pl.sda.purchase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.purchase.model.Item;
import pl.sda.purchase.model.Purchase;
import pl.sda.purchase.model.Request;
import pl.sda.purchase.model.dto.AddReguestDto;
import pl.sda.purchase.model.dto.ModifyItemDto;
import pl.sda.purchase.model.dto.ModifyRequestDto;
import pl.sda.purchase.service.ItemService;
import pl.sda.purchase.service.LoginService;
import pl.sda.purchase.service.PurchaseService;
import pl.sda.purchase.service.RequestService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.xml.soap.Detail;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/request/")
public class RequestController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private RequestService requestService;

    @GetMapping("/api/add")
    public String addDetail(Model model, @RequestParam(name = "id") Long id) {

        AddReguestDto addReguestDto = new AddReguestDto();
        List<Item> itemList = itemService.findAll();


        addReguestDto.setPurchaseId(id);

        model.addAttribute("item_list", itemList);

        model.addAttribute("new_request", addReguestDto);

        return "request/add_request";

    }

    @PostMapping("api/add")
    public String addDetailPost(Model model, AddReguestDto dto) {

        Optional<Request> optionalRequest = requestService.addRequest(dto);



        return "redirect:/order/get";
    }

    @GetMapping("/api/modify")
    public String modify(Model model, @RequestParam(name="id") Long id){
        Optional<Request> optionalRequest = requestService.getById(id);


        if(!optionalRequest.isPresent()){
            return "redirect:/order/get";
        }

        ModifyRequestDto dto = new ModifyRequestDto();
        dto.setRequestId(id);

        List<Item> itemList = itemService.findAll();


        model.addAttribute("item_list", itemList);
        model.addAttribute("modify_request", dto);

        return "request/modify";
    }

    @PostMapping("/api/modify")
    public String mofify(ModifyRequestDto dto){
        Optional<Request> optionalRequest = requestService.modifyRequest(dto);

        return "redirect:/order/get";
    }

    @GetMapping("/api/remove")
    public String remove(@RequestParam(name="id") Long id){
        requestService.removeRequest(id);

        return "redirect:/order/get";
    }
}
