package pl.sda.purchase.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.sda.purchase.model.AppUser;
import pl.sda.purchase.model.dto.RegisterUserDto;
import pl.sda.purchase.service.AppUserService;

import java.util.Optional;

@Component
@Lazy
public class DefaultUserCreator {

    @Autowired
    public DefaultUserCreator(AppUserService appUserService) {
        Optional<AppUser> clientOptional = appUserService.findByEmail("4dm1n@localhost");
        if (!clientOptional.isPresent()) {


            RegisterUserDto dto = new RegisterUserDto();

            dto.setName("Admin");
            dto.setSurname("Admin");
            dto.setPassword("admin");
            dto.setPasswordConfirm("admin");
            dto.setEmail("admin@email.com");

            appUserService.addUser(dto);
        }
    }
}
