package pl.sda.purchase.service;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.purchase.model.AppUser;
import pl.sda.purchase.model.dto.RegisterUserDto;
import pl.sda.purchase.repository.AppUserRepository;

import java.util.Optional;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Optional<AppUser> addUser(RegisterUserDto dto) {

        if (dto.getPassword().equals(dto.getPasswordConfirm()) &&
                !appUserRepository.findByEmail(dto.getEmail()).isPresent()) {

            AppUser appUser = new AppUser();

            appUser.setName(dto.getName());
            appUser.setSurname(dto.getSurname());
            appUser.setPassword(encoder.encode(dto.getPassword()));
            appUser.setEmail(dto.getEmail());

            appUser = appUserRepository.save(appUser);

            return Optional.of(appUser);
        }
        return Optional.empty();
    }


    public Optional<AppUser> findByEmail(String email) {
        return appUserRepository.findByEmail(email);
    }

}
