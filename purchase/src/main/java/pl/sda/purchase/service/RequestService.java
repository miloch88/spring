package pl.sda.purchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.purchase.model.AppUser;
import pl.sda.purchase.model.Item;
import pl.sda.purchase.model.Purchase;
import pl.sda.purchase.model.Request;
import pl.sda.purchase.model.dto.AddReguestDto;
import pl.sda.purchase.model.dto.ModifyRequestDto;
import pl.sda.purchase.repository.AppUserRepository;
import pl.sda.purchase.repository.ItemRepository;
import pl.sda.purchase.repository.PurchaseRepository;
import pl.sda.purchase.repository.RequestRepository;

import java.util.List;
import java.util.Optional;

@Service
public class RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private ItemService itemService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private ItemRepository itemRepository;

    public Optional<Request> addRequest(AddReguestDto dto) {

        if (dto.getQuantity() != null) {
            Request request = new Request();
            Purchase purchase = purchaseService.findById(dto.getPurchaseId()).get();
            Item item = itemService.getById(dto.getItemId()).get();
            AppUser appUser = loginService.getLoggedInUser().get();

            request.setItem(item);
            request.setAppUser(appUser);
            request.setPurchase(purchase);
            request.setQuantity(Integer.parseInt(String.valueOf(dto.getQuantity())));
            request = requestRepository.save(request);

            appUser.getRequestList().add(request);
            appUserRepository.save(appUser);

            purchase.getRequestList().add(request);
            purchaseRepository.save(purchase);

            item.getRequestList().add(request);
            itemRepository.save(item);

            return Optional.of(request);
        }
        return Optional.empty();
    }


    public List<Request> getAll() {
        return requestRepository.findAll();
    }


    public Optional<Request> getById(Long id) {
        return requestRepository.findById(id);
    }

    public Optional<Request> modifyRequest(ModifyRequestDto dto) {
        Optional<Request> optionalRequest = requestRepository.findById(dto.getRequestId());
        if (optionalRequest.isPresent()) {

            Request request = optionalRequest.get();
            Item previousItem = request.getItem();
            Item item = itemService.getById(dto.getItemId()).get();

            request.setQuantity(Integer.parseInt(String.valueOf(dto.getQuantity())));

            previousItem.getRequestList().remove(request);
//            item.getRequestList().remove(request);
//
            request.setItem(item);
//
            item.getRequestList().add(request);

            request = requestRepository.save(request);

            return Optional.of(request);
        }
        return Optional.empty();


    }

    public boolean removeRequest(Long id) {
        if(requestRepository.existsById(id)){
            Request request = requestRepository.findById(id).get();

            //Usuwa request z requestList z Purchase
            purchaseService.findById(request.getPurchase().getId()).get().getRequestList().remove(request);
            itemService.getById(request.getItem().getId()).get().getRequestList().remove(request);
            loginService.getLoggedInUser().get().getRequestList().remove(request);

            requestRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
