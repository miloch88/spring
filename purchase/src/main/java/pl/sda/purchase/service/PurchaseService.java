package pl.sda.purchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.purchase.model.Purchase;
import pl.sda.purchase.model.Request;
import pl.sda.purchase.model.dto.AddPurchaseDto;
import pl.sda.purchase.repository.PurchaseRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private LoginService loginService;

    @Autowired
    private RequestService requestService;

    public Optional<Purchase> addPurchase(AddPurchaseDto dto) {

        if (dto.getName() != null) {

            Purchase purchase = new Purchase();

            purchase.setName(dto.getName());
            purchase.setBegining(LocalDateTime.now());
            purchase.setWhoCreate(loginService.getLoggedInUser().get().getId());

            purchase = purchaseRepository.save(purchase);

            return Optional.of(purchase);
        }
        return Optional.empty();
    }

    public List<Purchase> getAll() {
        return purchaseRepository.findAll();
    }

    public Optional<Purchase> findById(Long id) {
        return purchaseRepository.findById(id);
    }

    public boolean removePurchase(Long id) {
        Optional<Purchase> optionalPurchase = purchaseRepository.findById(id);
        if (optionalPurchase.isPresent()) {

            Purchase purchase = optionalPurchase.get();
            List<Request> requestList = purchase.getRequestList();

            for (Request r : requestList) {
                requestService.removeRequest(r.getId());
            }

            purchaseRepository.deleteById(id);
            return true;
        }

        return false;

    }

    public Optional<Purchase> closePurchase(Long id) {
        Optional<Purchase> optionalPurchase = purchaseRepository.findById(id);

        if (optionalPurchase.isPresent()) {
            Purchase purchase = optionalPurchase.get();

            purchase.setClose(LocalDateTime.now());
            purchase = purchaseRepository.save(purchase);
            return Optional.of(purchase);
        }
        return Optional.empty();
    }
}
