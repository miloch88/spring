package pl.sda.purchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.sda.purchase.component.DefaultUserCreator;
import pl.sda.purchase.model.AppUser;

import java.util.Optional;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private AppUserService appUserService;

    //Musi być aby zainicjalizować @Lazy w DefaultUserCreator
    @Autowired
    private DefaultUserCreator defaultUserCreator;

    //Ładowane automatycznie przez implementację UseDetailsService
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<AppUser> optionalAppUser = appUserService.findByEmail(email);

        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();

            //Nadajemy adminowi wartości
            if (email.equals("admin@email.com")) {
                return User.withUsername(appUser.getEmail()).password(appUser.getPassword())
                        .roles("ADMIN", "USER").build();
            }
            return User.withUsername(appUser.getEmail()).password(appUser.getPassword())
                    .roles("USER").build();
        }
        throw new UsernameNotFoundException("User not found by name: " + email);
    }

    public Optional<AppUser> getLoggedInUser() {
        //Pobiera zalogowanego użytkownika
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return appUserService.findByEmail(user.getUsername());
        }
        return Optional.empty();
    }
}

