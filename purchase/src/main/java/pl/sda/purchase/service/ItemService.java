package pl.sda.purchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.purchase.model.Item;
import pl.sda.purchase.model.Store;
import pl.sda.purchase.model.dto.AddItemToStoreDto;
import pl.sda.purchase.model.dto.ModifyItemDto;
import pl.sda.purchase.repository.ItemRepository;
import pl.sda.purchase.repository.StoreRespository;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private StoreService storeService;

    @Autowired
    private StoreRespository storeRespository;

    public Optional<Store> addItemToStore(AddItemToStoreDto dto) {
        return addItemToStore(dto.getName(), dto.getPrice(), dto.getUrl(), dto.getStoreId());
    }

    private Optional<Store> addItemToStore(String name, Double price, String url, Long storeId) {
        Optional<Store> optionalStore = storeRespository.findById(storeId);
        if (optionalStore.isPresent()) {
            Store store = optionalStore.get();

            Item item = new Item();

            item.setName(name);
            item.setPrice(Double.parseDouble(String.valueOf(price)));
            item.setUrl(url);

            item.setStore(store);
            item = itemRepository.save(item);

            store.getItemList().add(item);
            store = storeRespository.save(store);

            return Optional.of(store);
        }
        return Optional.empty();
    }

    public List<Item> findAll() {
        return itemRepository.findAll();
    }

    public Optional<Item> getById(Long id) {
        return itemRepository.findById(id);
    }

    public Optional<Item> modifyItem(ModifyItemDto dto) {
        Optional<Item> optionalItem = itemRepository.findById(dto.getItemId());
        if (optionalItem.isPresent()) {
            Store store = storeService.getById(dto.getStoreId()).get();
            Item item = optionalItem.get();

            store.getItemList().remove(item);

            if (dto.getName() != null) {
                item.setName(dto.getName());
            }

            if (dto.getPrice() != null) {
                item.setPrice(Double.parseDouble(String.valueOf(dto.getPrice())));
            }

            if (dto.getUrl() != null) {
                item.setUrl(dto.getUrl());
            }

            item.setStore(store);
            item = itemRepository.save(item);

            store.getItemList().add(item);
            store = storeRespository.save(store);

            return Optional.of(item);
        }
        return Optional.empty();
    }

    public boolean removeItem(Long id) {
        if(itemRepository.existsById(id)){
        itemRepository.deleteById(id);
        return true;
        }
        return false;
    }
}
