package pl.sda.purchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.sda.purchase.model.Item;
import pl.sda.purchase.model.Store;
import pl.sda.purchase.model.dto.AddItemToStoreDto;
import pl.sda.purchase.model.dto.AddStoreDto;
import pl.sda.purchase.model.dto.ModifyStoreDto;
import pl.sda.purchase.repository.ItemRepository;
import pl.sda.purchase.repository.StoreRespository;

import java.util.List;
import java.util.Optional;

@Service
public class StoreService {

    @Autowired
    private StoreRespository storeRespository;



    public Optional<Store> addStore(AddStoreDto dto) {

        if(!storeRespository.findByName(dto.getName()).isPresent()){

            Store store = new Store();

            store.setName(dto.getName());

            store = storeRespository.save(store);

            return Optional.of(store);
        }
        return Optional.empty();
    }

    public List<Store> getAllStore() {
        return storeRespository.findAll();
    }

    public boolean removeStore(Long id) {
        if(storeRespository.existsById(id)) {
         storeRespository.deleteById(id);
         return true;
        }
        return false;
    }

    public Optional<Store> getById(Long id) {
        return storeRespository.findById(id);
    }

    public Optional<Store> modifyStore(ModifyStoreDto dto) {
        Optional<Store> optionalStore = storeRespository.findById(dto.getStoreId());
        if(optionalStore.isPresent()){
            Store store = optionalStore.get();

            if(dto.getName() != null){
                store.setName(dto.getName());
            }

            store = storeRespository.save(store);
            return Optional.of(store);
        }
        return Optional.empty();
    }


}
