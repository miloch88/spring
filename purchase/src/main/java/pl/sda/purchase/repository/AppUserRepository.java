package pl.sda.purchase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.purchase.model.AppUser;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByEmail(String email);
}
