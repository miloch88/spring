package pl.sda.purchase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.purchase.model.Item;

public interface ItemRepository extends JpaRepository<Item, Long > {
}
