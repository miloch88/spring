package pl.sda.purchase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.purchase.model.Purchase;

public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
}
