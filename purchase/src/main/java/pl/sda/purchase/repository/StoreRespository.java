package pl.sda.purchase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.purchase.model.Store;

import java.util.Optional;

public interface StoreRespository extends JpaRepository<Store, Long> {
    Optional<Store> findByName(String name);
}
