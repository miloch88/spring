package pl.sda.purchase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.purchase.model.Request;

public interface RequestRepository extends JpaRepository<Request, Long> {
}
