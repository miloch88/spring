package com.example.orders.service;

import com.example.orders.component.DefaultUserCreator;
import com.example.orders.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private DefaultUserCreator defaultUserComponent;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Employee> employeeOptional = employeeService.findByEmail(email);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();

            if (email.equals("admin@email.com")) {
                return User.withUsername(employee.getEmail()).password(employee.getPassword())
                        .roles("ADMIN", "USER").build();
            }
            return User.withUsername(employee.getEmail()).password(employee.getPassword())
                    .roles("USER").build();
        }
        throw new UsernameNotFoundException("User not found by name: " + email);
    }

    public Optional<Employee> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return employeeService.findByEmail(user.getUsername());
        }
        return Optional.empty();
    }
}
