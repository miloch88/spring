package com.example.orders.service;

import com.example.orders.model.Employee;
import com.example.orders.model.dto.AddNewEmplyeeDto;
import com.example.orders.respository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Optional<Employee> addEmployee(AddNewEmplyeeDto dto) {
        if(dto.getPassword().equals(dto.getPasswordConfirm()) &&
        !employeeRepository.findByEmail(dto.getEmail()).isPresent()){

            Employee employee = new Employee();

            employee.setName(dto.getName());
            employee.setSurname(dto.getSurname());
            employee.setEmail(dto.getEmail());
            employee.setPassword(encoder.encode(dto.getPassword()));

            employee = employeeRepository.save(employee);

            return Optional.of(employee);
        }
        return Optional.empty();
    }

    public Optional<Employee> findByEmail(String email) {
        return employeeRepository.findByEmail(email);
    }
}
