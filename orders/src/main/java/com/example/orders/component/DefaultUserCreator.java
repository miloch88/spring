package com.example.orders.component;

import com.example.orders.model.Employee;
import com.example.orders.model.dto.AddNewEmplyeeDto;
import com.example.orders.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Lazy
public class DefaultUserCreator {


    @Autowired
    public DefaultUserCreator(EmployeeService employeeService) {
        Optional<Employee> clientOptional = employeeService.findByEmail("admin@email.com");
        if (!clientOptional.isPresent()) {

            AddNewEmplyeeDto addNewEmplyeeDto = new AddNewEmplyeeDto();

            addNewEmplyeeDto.setEmail("admin@email.com");
            addNewEmplyeeDto.setPassword("admin");
            addNewEmplyeeDto.setPasswordConfirm("admin");
            addNewEmplyeeDto.setName("AAA");
            addNewEmplyeeDto.setSurname("BBB");

            employeeService.addEmployee(addNewEmplyeeDto);
        }
    }
}