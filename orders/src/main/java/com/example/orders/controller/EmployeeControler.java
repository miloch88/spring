package com.example.orders.controller;

import ch.qos.logback.core.net.server.Client;
import com.example.orders.model.Employee;
import com.example.orders.model.dto.AddNewEmplyeeDto;
import com.example.orders.service.EmployeeService;
import com.example.orders.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping ("/")
public class EmployeeControler {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/login")
    public String getLoginView(){
        return "admin/login_form";
    }

    @GetMapping("/")
    public String getIndex(Model model){
        Optional<Employee> employeeOptional = loginService.getLoggedInUser();
        if(employeeOptional.isPresent()){
            Employee employee = employeeOptional.get();

            model.addAttribute("username", employee.getEmail());
        }
        return "redirect:/";
    }


    @GetMapping("/add")
    public String addEmployee(Model model){
        model.addAttribute("new_employee", new AddNewEmplyeeDto());
        return "admin/add_employee_form";
    }

    @PostMapping("/add")
    public String addEmployeePost(Model model, AddNewEmplyeeDto dto){
        if(dto.getPassword().isEmpty() || dto.getPassword().length() < 3){
            model.addAttribute("new_employee", dto);
            model.addAttribute("error_message", "Password is incorrect");
            return "admin/add_employe_form";
        }

        Optional<Employee> employeeOptional = employeeService.addEmployee(dto);
        if(!employeeOptional.isPresent()){
            model.addAttribute("new_employe", dto);
            model.addAttribute("error_message", "User with that email already exists");
            return "admin/employe_form";
        }
        return "redirect:/";

    }
}
